+++
title = "Premier article, un peu de contexte"
author = "BALLAN Romain"
date = 2020-05-17T12:23:38+02:00
draft = false
+++


# A propos de moi

Cela fait maintenant quelques années que je travaille dans l'IT. Initialement arrivé en tant qu'ingénieur systèmes & réseaux à l'issue de ma formation, à une époque où la virtualisation était reine et de la "sacro-sainte" VM (en 5 ans il s'en passe des choses en informatique). 

De nouvelles technologies sont apparues (les conteneurs, orchestrateurs,..) et celles-ci ont révolutionné les façons de faire et les méthodes de travail. Au fur et à mesure du temps, et de changements, j'ai ainsi peu à peu migré vers un poste de type SRE (Site Reliability Engineer) à l'heure où les VMs ne sont plus que du bétail qu'on détruit et recrée à l'aide de grands coups de pipelines et d'Infrastructure As Code. 

Dans le cadre de mon job actuel, j'ai la chance en tant qu'Ops de travailler au quotidien avec des développeurs (DevOps toussa toussa). Je bosse majoritairement sur des technologies issues du monde Linux, _on premises_ sur un cloud privé OpenStack. Je commence peu à peu à m'ouvrir sur le cloud public. 
Je suis également un utilisateur d'Ubunbu (que je n'échangerai contre rien au monde pour travailler).

A titre personnel, j'adore également "bidouiller" en domotique, la blockchain et donc les cryptomonnaies. Il n'est donc pas exclu à l'avenir que des articles parlent de ces sujets qui me tiennent à cœur. 

# Qu'est-ce qui va se trouver sur ce blog ?

Comme vous pouvez donc vous en douter, les articles que vous trouverez sur ce blog vont aborder les domaines suivants (liste non exhaustive 👍) : 

- tout ce qui touche de près ou de loin à de l'administration système (scripting virtualisation, conteneurisation,...)
- les technologies DevOps
- l'automatisation (scripting, CI/CD,...)
- mes centres d'intérêts
- ... 

Mais vous l'aviez déjà deviné n'est-ce pas 😆 ? 

![Thx Captain](https://media.giphy.com/media/l0IpXMhfUNYtlFqnu/giphy.gif) 

PS: Ah oui j'allais oublier, j'aime également beaucoup les emojis et les GIFs !! 

A très vite pour la suite 🔥

