+++
title = "Création du Blog : choix de l'outillage (Partie 1)"
date = 2020-05-17T12:32:18+02:00
draft = false
+++

# Comment j'ai créé mon blog :construction:? 

Dans cet article (qui sera décliné en plusieurs parties) je vais vous parler de la création de ce blog dans l'objectif de répondre à la question suivante :  

Quelles sont les technologies utilisées et pourquoi j'ai choisi de les mettre en oeuvre ?

# Identifier un outil pouvant correspondre à mon besoin 

Avec ce blog, j'ai tenté de rester fidèle à moi même et donc je me suis efforcé de trouver le meilleur outil pouvant correspondre à mon besoin 🤘.
En effet, de nos jours et surtout dans le monde de l'informatique trouver un outil n'est pas un problème. Néanmoins trouver **l'outil** peut être un plus compliqué. 

Pour cela j'ai donc essayé d'exprimer au mieux mon besoin qui est : 

* Pouvoir publier des articles facilement Internet
* Être en mesure de supporter et gérer les commentaires 
* Simple de prise en main
* Doit être léger, ne pas consommer trop de ressources  

![](https://media.giphy.com/media/3oKIPqsXYcdjcBcXL2/giphy.gif)

## Site dynamique ou statique ?

Il existe plusieurs solutions pour héberger un site web ou un blog. 

Les sites statiques sont construits à un instant T à partir de sources, puis sont publiés sur un serveur web afin d'être accessibles. Tout changement nécessite une construction et livraison de celui-ci. Étant donné qu'ils sont statiques, il sont que très peu vulnérables aux attaques. 

Les sites dynamiques sont souvent couplés à une basse de données et sont modifiables à la volée directement en ligne. 

Un des plus connus est **WordPress**, que j'ai eu l'occasion de manipuler par le passé. C'est une solution plutôt simple à installer, complête et grandement personnalisable (beaucoup de thèmes et modules disponibles).
L'édition des articles se fait via la partie d'administration de l'application et il n'est pas possible de travailler en mode hors ligne.

Dans le cadre de mon activité professionnelle, j'ai beaucoup travaillé avec **Asciidoc**, c'est un langage permettant de faire de la saisie de notes et qui peut derrière être généré sous diverses formes (PDF, présentation, site HTML statique,..). Cela permet de ce concentrer sur le contenu plutôt que sur la forme tout en pouvant générer plusieurs formats de sortie à partir des mêmes sources. **Markdown** est un format très similaire et beaucoup plus répendu car il est entre autres utilisé pour rédiger les documentations sur les SCM comme GitHub ou GitLab.  

En l'état et en accord avec mon besoin, j'ai préféré partir sur un site statique pour l'hébergement de ce site. 

## Hugo : un choix plutôt naturel

Hugo se décrit comme générateur de site web statique créé pour rendre la création web facile.
Écrit en language *Golang*, il permet à partir d'entrants (contenu) de générer un site internet qui peut ensuite être hébergé sur n'importe quel serveur web (nginx, apache2,...). Un point important si on regarde mon besoin exprimé plus haut :white_check_mark:. 

En plus de permettre la génération d'un site en local, il permet également de gérer les brouillons, d'intégrer des commentaires :white_check_mark:, d'adopter des thèmes mis à disposition sur internet et bien plus encore. 

Relativement répandu sur la toile et disposant d'une importante communauté, cela en fait un outil idéal pour correspondre à mon besoin.

Vous trouverez plus d'informations à son sujet [ici](https://gohugo.io/about/).

La suite au prochain épisode :fire: ! 

