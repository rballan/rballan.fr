+++
title = "Création du Blog : Choix de l'outillage (Partie 2)"
date = 2021-02-25T17:20:11+01:00
draft = false
+++

# La suite de la boite à outils 🧰

Après une ~~petite~~ longue pause sur ce sujet, je reviens pour vous partager la suite de la série d'articles précédemment initiée.

Dans le précédent billet, je vous ai expliqué quel outil j'ai choisi d'utiliser pour la gestion du ce blog : Hugo. 
Et cette fois, on va parler de tout le reste en fait ! Car il y en a quand même un certain nombre !

## L'hébergement ⚙️ 

Et bien oui, faut bien héberger tout ça sur internet, je ne vais quand même pas laisser tourner mon ordinateur 24/7 ... 👎 

### Le serveur 🔧

Disposant déjà d'un VPS chez OVH, j'ai décidé de ré-utiliser celui-ci dans l'optique d'héberger ce blog. 
En effet, un site statique généré avec `HUGO` ne nécessite que de très peu de ressources pour tourner (qu'un serveur web en fait).

Côté sécurité 🛡️, j'ai choisi d'ouvrir uniquement les ports essentiels (22, 80, 443) au niveau du réseau au travers le pare-feu proposé par OVH.

En parlant de ça, je recommande également fortement l'installation de `FailToBan` afin de renforcer la sécurité de votre serveur (pour les connexions SSH notamment). 
Car oui, si ça ne vous est jamais arrivé je vous invite à regarder les logs de connexions SSH sur vos serveurs accessibles directement sur Internet ! 

![Security first!](https://media.giphy.com/media/VGwTq3G6a39cI/giphy.gif)

### Le serveur web 🗺️

Dans mon cas, j'ai choisi d'utiliser `Apache` pour le serveur web. Son travail est pluto simple, rendre possible l'accès aux fichiers générés par HUGO. 

C'est donc lui qui va permettre l'accès au blog, et ceci tout en forçant la redirection des flux HTTP => HTTPS.

### La gestion des certificats 🔗

`Let's Encrypt` est une autorité de certification qui fournit des certificats gratuits à la norme X509. 
De nos jours il est très simple avec cette autorité de générer des certificats ayant une durée de vie de 3 mois qu'il est possible de renouveler. 

Un certificat valide permet d'appliquer de l'`HTTPS` et d'éviter ainsi les avertissements des navigateurs internet. 

Il est possible de générer tout ça à la main, mais étant un peu feignant, je privilégie l'automatisation (car refaire la manipulation chaque trimestre est quelque peu ennuyeux :flushed:). 
C'est pourquoi j'ai utilisé `Certbot` qui est compatible avec `Apache` et permet de gérer le cycle de vie des certificats automatiquement. 

## Outillage 🧰 

### La gestion du code source 📒

Alors pourquoi un gestionnaire de code source me direz-vous ? Car étant donné que c'est un site statique et que ce que je vais produire pour l'alimenter s'apparente à du code (Markdown) il faut pouvoir le stocker et surtout le versionner. 

Je ne fais pas dans l'originalité de ce côté-là. En effet, il existe deux outils principaux : 

* GitHub 
* GitLab 

Les deux peuvent répondre au besoin de base, néanmoins avec GitLab il est possible d'utiliser `GitLab CI` pour permettre d'automatiser tout un tas de choses, ce qui peut s'avérer très pratique. C'est donc lui que j'ai choisi d'utiliser pour héberger mon code.

Nous reviendrons sur ce point dans un futur article où je vous présenterai comment j'ai mis en œuvre le déploiement automatique des nouveaux articles sur le blog. 

### Docker

Si j'étais moins feignant, je n'utiliserai pas `Docker`. Mais j'ai tendance à penser que s'il est nécessaire de faire une même opération manuelle plus de trois fois, c'est qu'il faut l'automatiser. 

Dans mon contexte, cet outil est une brique de l'automatisation permettant de déployer de nouveaux articles sans même se connecter au serveur. La classe non ? 

![Automation first!](https://media.giphy.com/media/wRjrQJ57Yl3RC/giphy.gif)

La suite au prochain épisode 🔥 ! 