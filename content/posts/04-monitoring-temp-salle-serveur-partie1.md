+++
title = "DIY : Monitoring de température avec un Raspberry et capteur DHT22"
date = 2021-05-08T17:06:32+02:00
draft = false
+++

# Un peu de contexte

Dans le cadre de boulot actuel, nous hébergeons nos serveurs dans notre propre salle serveur.

Cette façon de faire a des avantages, mais également des inconvénients. Notamment sur tout ce qui touche à l'électricité et la climatisation. C'est sur ce second point que nous allons nous attarder dans cet article.

En effet, nous avons des sondes de températures sur les chassis des serveurs, mais rien permettant de remonter la température ambiante de la salle. L'idée est de pouvoir remonter en amont d'éventuels problèmes avec la climatisation au plus tôt. 

C'est pourquoi il est pertinent de monitorer la température ambiante ainsi que le taux d'humidité de la pièce. 

# Solutions envisagées

Il existe plusieurs solutions :  

* Utiliser les sondes du matériel existant (onduleurs,...)
* Acheter une sonde dédiée comme par exemple [ce modèle](https://instrumentys.com/produit/tsf02-rj45-transmetteur-de-temperature-humidite-ethernet/) dont le budget tourne autour des 200€
* Monter une solution "maison"  

La première option peut être viable, mais ne permet pas de remonter l'hygrométrie de la pièce.

La seconde option est peut être la plus "professionnelle", je dois l'avouer. Mais quid de l'exploitation des données ainsi collectées ?

Ayant un Raspberry Pi à disposition, j'ai donc regardé quelles étaient les possibilités afin de collecter les métriques souhaitées et il s'avère que pas mal de capteurs peuvent être exploités directement au travers des ports GPIO. De plus, leur prix est très intéressant.
L'avantage est qu'on peut presque faire ce que l'on veut avec les métriques ainsi collectées. On peut très bien imaginer que les information ainsi collectées sont envoyées sur le serveur supervision Zabbix, pour faire de jolis graphiques et envoyer des alertes si nécessaire.
Le capteur [DHT22](https://www.az-delivery.de/fr/products/dht22-temperatursensor-modul?variant=5595305476123) semble être idéal pour ce besoin.  

La suite de l'article se présente comme un how to.

# Installation de Raspberry Pi OS lite

* Télécharger l'image d'installation https://raspberry-pi.fr/download/raspbian_lite_latest.zip[ici]
* Utiliser l'utilitaire `startup disk creator` pour copier l'image sur la carte SD
* Insérer la carte SD dans le Raspberry Pi

---
**NOTE:**

Il n'y a pas de GUI avec la version lite de l'OS

---

# Initialisation du Raspberry Pi 

## Activation d'SSH (avec un écran) 

* Brancher le Raspberry Pi à un écran, et connecter un clavier dessus
* Se connecter avec le login / password par défaut : `pi / raspberry`

---
**ATTENTION:**

Le clavier est en QWERTY lors de la première connexion.

---

* Passer le clavier en AZERTY
  * Éditer le fichier `/etc/default/keyboard`
  * Éditer la ligne `XKBLAYOUT` en remplaçant le `gb` par `fr`
  * Sauvegarder le fichier puis redémarrer le Raspberry pour appliquer la modification 
* Activer l'accès SSH  
  * Lancer l'utilitaire de configuration Raspberry : `sudo raspi-config`
  * Aller dans `Interface Options`
  * Sélectionner `Enable/disable remote command line access using SSH`

## Activation d'SSH (sans écran)

Si pas d'écran à disposition il est possible d'activer le protocole SSH directement depuis la partition boot de la carte SD, pour cela :  

* Insérer la carte SD sur un PC
* Ouvrir la partition boot (la seule visible)
* Créer un fichier nommé `ssh` (sans extension ni contenu)
* Insérer la carte SD dans le Raspberry Pi et SSH devrait être activé au démarrage 

## Préparation  

Une fois connecté (en SSH ou au travers du CLI) : 

* Mettre à jour les paquets installés : `sudo apt update && sudo apt upgrade -y`
* Modifier le MDP du compte  `pi` (soit au travers du CLI raspi-config ou alors via un `passwd` en SSH)

# Installation du capteur DHT22

* Connecter le module en respectant le câblage préconisé par le constructeur :

| DHT22 pin  | Raspberry Pi pin label   | pin number |
|------------|--------------------------|------------|
| +          | 5v                       | 2          |
| out        | GPIO 12                  | 32         |
| -          | GND                      | 30         |

Un petit schéma, car faut avouer que c'est quand même plus pratique 👍 :  

![Schéma câblage GPIO DHT22](DHT22-cabling.png)

* Installation de pip3 pour récupérer la librairie `sudo apt install python3-pip -y`
* Récupération des dépendances pip utilisées plus tard : `sudo python3 -m pip install --upgrade pip setuptools wheel`
* Installer la librairie `Adafruit_DHT` : `sudo pip3 install Adafruit_DHT`
* Créer le script python `DHT22.py` suivant : 

```python
import Adafruit_DHT
from time import sleep
sensor = Adafruit_DHT.DHT22
# DHT22 sensor connected to GPIO12.
pin = 12
print("[press ctrl+c to end the script]")
try: # Main program loop
    while True:
        humidity, temperature = Adafruit_DHT.read_retry(sensor,
pin)
        sleep(2.5)
        if humidity is not None and     temperature is not None:
            print("Temp={0:0.1f}*C Humidity={1:0.1f}%"
                               .format(temperature, humidity))
        else:
            print("Failed to get reading. Try again!")
# Scavenging work after the end of the program
except KeyboardInterrupt:
    print("Script end!")
```

* Tester le script :  

```bash
pi@raspberrypi:~ $ pwd
/home/pi
pi@raspberrypi:~ $ ls -l
total 4
-rw-r--r-- 1 pi pi 637 May  8 13:32 DHT22.py
pi@raspberrypi:~ $ python3 DHT22.py
[press ctrl+c to end the script]
Temp=23.4*C Humidity=53.0%
Temp=22.0*C Humidity=57.8%
Temp=21.9*C Humidity=57.5%
Temp=21.8*C Humidity=57.5%
Temp=21.9*C Humidity=57.6%
Temp=21.9*C Humidity=57.5%
Temp=21.9*C Humidity=57.6%
Temp=21.9*C Humidity=57.5%
Temp=21.9*C Humidity=57.5%
^CScript end!
```

Le script python remonte bien les valeurs détectées par le capteur, tout est OK à ce niveau là.

# Remonter l'information collectée à Zabbix

Il serait possible de faire jouer le script par un cron et stocker les valeurs dans un fichier txt qui serait ensuite exploité au travers de SNMP.  

Mais vu que l'agent Zabbix est tout à fait à même de lancer un script Python et d'exploiter son résultat, autant en profiter et faire comme cela.  

## Adaptation du script de base  

Il n'est pas nécessaire d'avoir une boucle car c'est l'agent Zabbix qui va lancer le script de temps en temps et ainsi récupérer les valeurs nécessaires.
On en profite également pour retirer les messages d'informations. Du coup, il est nécessaire de re-travailler le script : 

Dans mon cas, j'ai nommé ce script `DHT22-zabbix.py`

```python
import Adafruit_DHT
from time import sleep
sensor = Adafruit_DHT.DHT22
# DHT22 sensor connected to GPIO12.
pin = 12
humidity, temperature = Adafruit_DHT.read_retry(sensor,pin)
if humidity is not None and     temperature is not None:
    print("Temp={0:0.1f}*C  Humidity={1:0.1f}%".format(temperature, humidity))
else:
    print("Failed to get reading. Try again!")
    sys.exit(1)
```

Avec ces modifications, le script remonte juste les valeurs au moment où il est exécuté : 

```bash
pi@raspberrypi:~ $ sudo python3 DHT22-zabbix.py 
Temp=22.2*C  Humidity=80.3%
pi@raspberrypi:~ $ 
```

Maintenant que le script final est prêt, on peut enchaîner avec la partie Zabbix.  

## Installation et configuration de l'agent Zabbix

* Installer l'agent Zabbix : `sudo apt install zabbix-agent -y`
* Modifier l'utilisateur `zabbix`  
  * Lui donner accès à un shell et créer son home  
    * Éditer le `/etc/passwd` pour rajouter tout cela : dans mon cas je passe de `zabbix:x109:114::/nonexistent:/usr/sbin/nologin` à `zabbix:x109:114::/home/zabbix:/bin/bash`
    * Créer le home et lui donner les droits : `sudo mkdir && sudo chown zabbix /home/zabbix`
    * Lui donner les droits de lancer python3 sans avoir à saisir de mot de passe : pour cela taper la commande `visudo` et ajouter la ligne `zabbix ALL=(ALL) NOPASSWD: /usr/bin/python3`
* Déplacer le script modifié dans le nouveau home Zabbix : `mv DHT22-zabbix.py /home/zabbix`

---
**NOTE:**

Les droits `sudo` sont nécessaires afin de pouvoir accéder aux GPIO, et donc pouvoir exploiter au capteur DHT22.  

---

* Tester que l'appel du script python avec l'utilisateur zabbix fonctionne bien :  

```bash
pi@raspberrypi:~ $ sudo -i
root@raspberrypi:~# su - zabbix
zabbix@raspberrypi:~$ pwd
/home/zabbix
zabbix@raspberrypi:~$ sudo python3 DHT22-zabbix.py 
Temp=23.5*C  Humidity=57.4%
```

Le script que l'agent Zabbix va utiliser fonctionne bien, et retourne les valeurs à l'instant T de la température et le taux d'humidité détecté.  

* Editer la configuration de l'agent afin de lui permettre d'exécuter le script et d'exploiter les valeurs collectées `sudo nano /etc/zabbix/zabbix_agentd.conf`
  * Ajouter à la fin du fichier de configuration : `UserParameter=dht.pull[*],sudo python3 /home/zabbix/DHT22-zabbix.py | awk -F[=*%] '{print $$$1}'`

Cette ligne de configuration permet à l'agent Zabbix de lancer le script permettre de choisir la valeur retournée : la température ou le taux d'humidité.

## Récupération des valeurs au travers de l'agent Zabbix

Pour cela, il est possible directement de tester depuis votre serveur Zabbix grâce à l'utilitaire `zabbix_get`.  

Sinon, il est possible d'installer sur le Raspberry Pi le paquet `zabbix-proxy-sqlite3` qui descend cet utilitaire.  

Testons cela :

```bash
pi@raspberrypi:~ $ zabbix_get -s 127.0.0.1 -k dht.pull[2]
22.4
pi@raspberrypi:~ $ zabbix_get -s 127.0.0.1 -k dht.pull[4]
57.5
```

Comme on peut le voir, la requête `dht.pull[2]` permet de remonter la température et la `dht.pull[4]` le taux d'humidité.  

Côté client tout est en place, il n'y a plus qu'à exploiter ces valeurs directement depuis le serveur Zabbix !  

# Sources 

https://zabbix.tips/monitor-temperature-and-humidity-zabbix/